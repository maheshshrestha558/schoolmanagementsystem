﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace schoolmanage.model
{
    public class SectionStudent
    {
        public int SectionStudentId { get; set; }
        public int SectionId { get; set; }
        public int StudentId { get; set; }
        public Student Student { get; set; }


    }
}
