﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace schoolmanage.model
{
    public class AssignGrade
    {
        public int AssignGradeId { get; set; }
        public int? GradeId { get; set; }
        public Grade? Grade { get; set; }
        public int? TeaceherId { get; set; }
        public Teacher? Teacher { get; set; }
    }
}
