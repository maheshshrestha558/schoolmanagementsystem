﻿using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace schoolmanage.model
{
    public class Student
    {
        
        public int StudentId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB {  get; set; }
        public DateTime DateofJoin { get; set; }
        public bool Selected { get; set; }
        [Unique]
        public string KeyId { get; set; }

        public ICollection<Enroll> YearlySession { get; set; }


    }
}
