﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace schoolmanage.model
{
    public  class Grade
    {
        public int GradeId { get; set; }
        public string Name { get; set; }
        [NotMapped]
        public ICollection<AssignGrade> AssignGrades { get; set; } = new HashSet<AssignGrade>();

        public ICollection<Enroll> Enrolls { get; set; } = new HashSet<Enroll>();

        public ICollection<GradeSubject> GradeSubjects { get; set; }
    }
}
