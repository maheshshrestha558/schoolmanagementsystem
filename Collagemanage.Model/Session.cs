﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace schoolmanage.model
{
    public class Session
    {
        public int SessionId { get; set; }
        public string start { get; set; }
        public string End { get; set; }

        public ICollection<Enroll> Enrolls{get; set;} =new HashSet<Enroll>();

        public ICollection<TeacherSession> teacherSessions { get; set; }=new HashSet<TeacherSession>();


    }
}
