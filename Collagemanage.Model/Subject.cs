﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace schoolmanage.model
{
    public class Subject
    {
        public int SubjectId { get; set; }
        public string Name { get; set; }
        [NotMapped]
        public ICollection<GradeSubject> GradeSubjects { get; set; } 

    }
}
