﻿using Collagemanage.Model;
using Microsoft.AspNetCore.Identity;
using schoolmanage.model;
using Schoolmanage.Repository.Repository;
using Schoolmanage.Utilities;


using Schoolmanage.ViewModel.Viewmodel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoolmanage.Services.Services
{
    public class StudentServices : IStudentServices
    {
        private UserManager<ApplicationUser> _userManager;
        private RoleManager<IdentityRole> _roleManager;
        private IUnitofWork _unitofwork;

        public StudentServices(IUnitofWork unitofwork, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _unitofwork = unitofwork;
            _roleManager = roleManager;
        }

        public async Task AddStudent(CreateStudentViewModel Student)
        {
            ApplicationUser appuser = new ApplicationUser()
            {
                UserName = Student.Email,
                Email = Student.Email
            };
            var result = await _userManager.CreateAsync(appuser, Student.Password);
            if (result.Succeeded)
            {
                if (!await _roleManager.RoleExistsAsync("Student"))
                {
                    await _roleManager.CreateAsync(new IdentityRole("Student"));

                }
                await _userManager.AddToRoleAsync(appuser, "Student");
            }
            Student.KeyId = appuser.Id;
            var model = new CreateStudentViewModel().ConvertModel(Student);
            await _unitofwork.GenreRepository<Student>().AddAsync(model);
            _unitofwork.Save();
        }

        public PageResult<StudentViewModel> GetAll(int pageNumber, int pageSize)
        {
            int totalCount = 0;
            List<StudentViewModel> vmList = new List<StudentViewModel>();
            try
            {
                int ExcludeRecords = (pageSize * pageNumber) - pageSize;
                var modelList = _unitofwork.GenreRepository<Student>().GetAll()
                    .Skip(ExcludeRecords).Take(pageSize).ToList();
                totalCount = _unitofwork.GenreRepository<Student>().GetAll().ToList().Count;
                vmList = ConvertModelToViewModelList(modelList);

            }
            catch (Exception e)
            {
                throw;
            }
            var result = new PageResult<StudentViewModel>
            {
                Data = vmList,
                TotalItems = totalCount,
                PageNumber = pageNumber,
                PageSize = pageSize

            };
            return result;
        }

        private List<StudentViewModel> ConvertModelToViewModelList(List<Student> modelList)
        {
            return modelList.Select(x => new StudentViewModel(x)).ToList();
        }

        public int GetAllStudent()
        {
            var totalCount = _unitofwork.GenreRepository<Student>().GetAll().ToList().Count;
            return totalCount;
        }
    }
}
