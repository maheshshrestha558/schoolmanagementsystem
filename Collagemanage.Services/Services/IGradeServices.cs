﻿using Schoolmanage.Utilities;
using Schoolmanage.ViewModel.Viewmodel;




namespace Schoolmanage.Services.Services
{
    public interface IGradeServices
    {
        Task Add(CreateGradeViewModel vm);
        int AddGradeWithStudent(GradeViewModel grade, int sessionId, List<int> StudentList);
        PageResult<GradeViewModel> GetAll(int pageNumber, int pageSize);
        IEnumerable<GradeViewModel> GetAll();
        GradeViewModel GetById(int gradeId);
    }
}
