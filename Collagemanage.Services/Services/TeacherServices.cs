﻿using Collagemanage.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using schoolmanage.model;
using Schoolmanage.Repository.Repository;
using Schoolmanage.Utilities;
using Schoolmanage.ViewModel.Viewmodel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoolmanage.Services.Services
{
    public class TeacherServices : ITeacherServices
    {
        private UserManager<ApplicationUser> _userManager;
        private RoleManager<IdentityRole> _roleManager;
        private IUnitofWork _unitofwork;

        public TeacherServices(IUnitofWork unitofwork,
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            _unitofwork = unitofwork;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public int AddSessionwithTeacher(int SessionId, List<int> teahcerIdsList)
        {
            int count = 0;
            var Session = _unitofwork.GenreRepository<Session>().GetByIdAsync(x => x.SessionId == SessionId, 
                include: x => x.Include(y => y.teacherSessions));

            foreach (var item in teahcerIdsList)
            {
                if (!_unitofwork.GenreRepository<TeacherSession>()
                    .Exists(x => x.SessionId == SessionId && x.TeacherId == item))
                {
                    Session.teacherSessions.Add(new TeacherSession()
                    {
                        TeacherId = item,
                        SessionId = SessionId
                    });
                    count++;

                }
            }
            _unitofwork.GenreRepository<Session>().Update(Session);
            _unitofwork.Save();
            return count;
        }

        public async Task AddTeacher(CreateTeacherViewModel vm)
        {
            ApplicationUser appUSer = new ApplicationUser()
            {
                UserName = vm.UserName,
                Email = vm.Email,
            };
            var result = await _userManager.CreateAsync(appUSer, vm.Password);
            if (result.Succeeded)
            {
                if (!await _roleManager.RoleExistsAsync("Teacher"))
                {
                    await _roleManager.CreateAsync(new IdentityRole("Teacher"));

                }
                await _userManager.AddToRoleAsync(appUSer, "Teacher");

            }
            vm.KeyId = appUSer.Id;
            var model = new CreateTeacherViewModel().ConvertModel(vm);
            await _unitofwork.GenreRepository<Teacher>().AddAsync(model);
            _unitofwork.Save();
        }

        public PageResult<TeacherViewModel> GetAllTeacher(int pageNumber, int pageSize)
        {
            int totalCount = 0;
            List<TeacherViewModel> vmList = new List<TeacherViewModel>();
            try
            {
                int ExcludeRecords = (pageSize * pageNumber) - pageSize;
                var modelList = _unitofwork.GenreRepository<Teacher>().GetAll()
                    .Skip(ExcludeRecords).Take(pageSize).ToList();
                totalCount = _unitofwork.GenreRepository<Teacher>().GetAll().ToList().Count;
                vmList = ConvertModelToViewModelList(modelList);

            }
            catch (Exception e)
            {
                throw;
            }
            var result = new PageResult<TeacherViewModel>
            {
                Data = vmList,
                TotalItems = totalCount,
                PageNumber = pageNumber,
                PageSize = pageSize

            };
            return result;
        }

        private List<TeacherViewModel> ConvertModelToViewModelList(List<Teacher> modelList)
        {
            return modelList.Select(x => new TeacherViewModel(x)).ToList();
        }

        public int GetAllTeacher()
        {
            var count=_unitofwork.GenreRepository<Teacher>().GetAll().ToList().Count;
            return count;
        }
    }
}
