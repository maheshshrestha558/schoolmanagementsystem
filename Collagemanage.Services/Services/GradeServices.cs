﻿using Microsoft.EntityFrameworkCore;
using schoolmanage.model;
using Schoolmanage.Repository.Repository;
using Schoolmanage.Utilities;
using Schoolmanage.ViewModel.Viewmodel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace Schoolmanage.Services.Services
{
    public  class GradeServices: IGradeServices
    {
        private IUnitofWork _unitofWork;

        public GradeServices(IUnitofWork unitofWork)
        {
            _unitofWork = unitofWork;
        }


         public async Task Add(CreateGradeViewModel vm)
        {
            var model= new CreateGradeViewModel().Convert(vm);
            await _unitofWork.GenreRepository<Grade>().AddAsync(model);
            _unitofWork.Save();
        }

        public int AddGradeWithStudent(GradeViewModel grade, int sessionId, List<int> StudentList)
        {
            int count = 0;
            var model = new GradeViewModel().Convert(grade);

            foreach(var item in StudentList)
            {
                if(!_unitofWork.GenreRepository<Enroll>().Exists(x=>x.SessionId==sessionId && x.StudentId==item))
                {
                    model.Enrolls.Add(new Enroll()
                    {
                        StudentId = item,
                        GradeId = grade.Id,
                        SessionId = sessionId
                    });
                    count++;

                }            
            }
            return count;
        }

        public PageResult<GradeViewModel> GetAll(int pageNumber, int pageSize)
        {
            int total = 0;
            List<GradeViewModel> vmlist = new List<GradeViewModel>();
            try
            {
                

                int ExcludeRecords = (pageSize * pageNumber) - pageSize;
                var modelList = _unitofWork.GenreRepository<Grade>().GetAll()
                    .Skip(ExcludeRecords).Take(pageSize).ToList();
                total = _unitofWork.GenreRepository<Student>().GetAll().ToList().Count;
                vmlist = ConvertModelToViewModelList(modelList);



            }
            catch(Exception ex) { throw; }

            var result = new PageResult<GradeViewModel>
            {
                Data = vmlist,
                TotalItems=total,
                PageNumber=pageNumber,
                PageSize=pageSize   

            };
            return result;
        }

        private List<GradeViewModel> ConvertModelToViewModelList(List<Grade> modelList)
        {
            return modelList.Select(x => new GradeViewModel(x)).ToList();

        }

        public IEnumerable<GradeViewModel> GetAll()
        {

            var modellist = _unitofWork.GenreRepository<Grade>().GetAll();
            var vmlist = ConvertModelToViewModelList(modellist.ToList());
            return vmlist;


        }

        public GradeViewModel GetById(int gradeId)
        {
            var model = _unitofWork.GenreRepository<Grade>().GetByIdAsync(x => x.GradeId == gradeId,include: y=>y.Include(a=>a.Enrolls));
            var vm = new GradeViewModel(model);
            return vm;


        }

    }
}
