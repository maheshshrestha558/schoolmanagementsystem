﻿using schoolmanage.model;
using Schoolmanage.Repository.Repository;
using Schoolmanage.Utilities;
using Schoolmanage.ViewModel.Viewmodel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoolmanage.Services.Services
{
    public class SessionServices: ISessionServices
    {
        private IUnitofWork _unitofWork;

        public SessionServices(IUnitofWork unitofWork)
        {
            _unitofWork = unitofWork;
        }

        public async Task Add(CreateSessionViewModel vm)
        {
            var model = new CreateGradeViewModel().Convert(vm);
            await _unitofWork.GenreRepository<Session>().AddAsync(model);
            _unitofWork.Save();

        }

        public PageResult<SessionViewModel> GetAll(int pageNumber, int pageSize)
        {
            int totalCount = 0;
            List<SessionViewModel> vmList = new List<SessionViewModel>();
            try
            {
                int ExcludeRecords = (pageSize * pageNumber) - pageSize;
                var modelList = _unitofWork.GenreRepository<Session>().GetAll()
                    .Skip(ExcludeRecords).Take(pageSize).ToList();
                totalCount = _unitofWork.GenreRepository<Session>().GetAll().ToList().Count;
                vmList = ConvertModelToViewModelList(modelList);

            }
            catch (Exception e)
            {
                throw;
            }
            var result = new PageResult<SessionViewModel>
            {
                Data = vmList,
                TotalItems = totalCount,
                PageNumber = pageNumber,
                PageSize = pageSize

            };
            return result;
        }

        private List<SessionViewModel> ConvertModelToViewModelList(List<Session> modelList)
        {
            return modelList.Select(x => new SessionViewModel(x)).ToList();
        }

        public IEnumerable<SessionViewModel> GetAll()
        {
            var modellist = _unitofWork.GenreRepository<Session>().GetAll();
            var vmlist = ConvertModelToViewModelList(modellist.ToList());
            return vmlist;
        }

        public SessionViewModel GetById(int sessionId)
        {
            return new SessionViewModel();
        }


    }
}
