﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Schoolmanage.Utilities;
using Schoolmanage.ViewModel.Viewmodel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoolmanage.Services.Services
{
    public interface ITeacherServices
    {
        int AddSessionwithTeacher(int SessionId, List<int> teahcerIdsList);
        Task AddTeacher(CreateTeacherViewModel vm);
        PageResult<TeacherViewModel> GetAllTeacher(int pageNumber,int  pageSize);

        int GetAllTeacher();
    }
}
