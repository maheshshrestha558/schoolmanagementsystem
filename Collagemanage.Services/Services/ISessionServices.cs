﻿using Schoolmanage.Utilities;
using Schoolmanage.ViewModel.Viewmodel;

namespace Schoolmanage.Services.Services
{
    public interface ISessionServices
    {
        Task Add(CreateSessionViewModel vm);

        PageResult<SessionViewModel> GetAll(int pageNumber, int pageSize);
        IEnumerable<SessionViewModel> GetAll();
        SessionViewModel GetById(int sessionId);
    }
}