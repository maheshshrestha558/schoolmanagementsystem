﻿using Schoolmanage.Utilities;
using Schoolmanage.ViewModel.Viewmodel;

namespace Schoolmanage.Services.Services
{
    public interface IStudentServices
    {
        Task AddStudent(CreateStudentViewModel Student);
        PageResult<StudentViewModel> GetAll(int pageNumber, int pageSize);

        int GetAllStudent();
    }
} 