using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Collagemanage.Model;
using Collagemanage.Repositoy;
using Schoolmanage.Utilities;
using Schoolmanage.Repository.Repository;
using Microsoft.AspNetCore.Identity.UI.Services;
using ServiceStack;
using Schoolmanage.Services.Services;
var builder = WebApplication.CreateBuilder(args);
var connectionString = builder.Configuration.GetConnectionString("Collagemanagement") ?? throw new InvalidOperationException("Connection string 'Collagemanagement' not found.");

builder.Services.AddDbContext<Applicationdbcontext>(options => options.UseSqlServer(connectionString));

builder.Services.AddIdentity<ApplicationUser,IdentityRole>().AddDefaultTokenProviders()
    .AddEntityFrameworkStores<Applicationdbcontext>();

builder.Services.AddScoped<IDbInitializer, DbInitializer>();
builder.Services.AddScoped<IUnitofWork, UnitofWork>();
builder.Services.AddScoped<IEmailSender, EmailSender>();
builder.Services.AddScoped<IStudentServices, StudentServices>();
builder.Services.AddScoped<ISessionServices, SessionServices>();
builder.Services.AddScoped<IGradeServices, GradeServices>();
builder.Services.AddScoped<ITeacherServices, TeacherServices>();






// Add services to the container.
builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();
DataSeeding();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

//app.UseEndpoints(endpoints =>
//{
//    endpoints.MapControllerRoute(
//      name: "areas",
//      pattern: "{area:exists}/{controller=Student}/{action=Index}/{id?}"
//    );
//});

app.Run();

void DataSeeding()
{
    using (var scope = app.Services.CreateScope())
    {
        var DbInitializer = scope.ServiceProvider.GetRequiredService<IDbInitializer>();
        DbInitializer.Initialize();
    }
}

