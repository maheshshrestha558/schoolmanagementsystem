﻿using Microsoft.AspNetCore.Mvc;
using Schoolmanage.Services.Services;
using Schoolmanage.Utilities;
using Schoolmanage.ViewModel.Viewmodel;

namespace Collagemanagement.web.Controllers
{
    [Area("Admin")]
    public class SessionsController : Controller
    {
        private ISessionServices _sessionSerices;

        public SessionsController(ISessionServices sessionSerices)
        {
            _sessionSerices = sessionSerices;
        }

        public IActionResult Index(int pageNumber = 1, int pageSize = 10)
        {
            PageResult<SessionViewModel> session = _sessionSerices.GetAll(pageNumber, pageSize);
            return View(session);
        }

        [HttpGet]

        public IActionResult Create(int id)
        {
            CreateSessionViewModel vm = new CreateSessionViewModel();
            vm.start = DateTime.Now.Year.ToString();
            var afteryear = DateTime.Now.AddYears(1);
            vm.end = afteryear.Year.ToString();
            return View(vm);



        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateSessionViewModel vm)
        {
            if (vm == null)
            {
                return View();

            }
            await _sessionSerices.Add(vm);
            return RedirectToAction("Index");
        }
    }
}
