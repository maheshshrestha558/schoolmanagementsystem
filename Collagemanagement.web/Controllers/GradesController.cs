﻿using Microsoft.AspNetCore.Mvc;
using Schoolmanage.Services.Services;
using Schoolmanage.Utilities;
using Schoolmanage.ViewModel.Viewmodel;

namespace Collagemanagement.web.Controllers
{
    [Area("Admin")]

    public class GradesController : Controller
    {
        private IGradeServices _gradeServices;

        public GradesController(IGradeServices gradeServices)
        {
            _gradeServices = gradeServices;
        }

        public IActionResult Index(int pageNumber = 1, int pageSize = 10)
        {
            PageResult<GradeViewModel> grades = _gradeServices.GetAll(pageNumber, pageSize);
            return View(grades);
        }
        public IActionResult AddGrade()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> AddGrade(CreateGradeViewModel gm)
        {
            await _gradeServices.Add(gm);
            return RedirectToAction("Index");
        }
    }
}
