﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Schoolmanage.Services.Services;
using Schoolmanage.Utilities;
using Schoolmanage.ViewModel.Viewmodel;

namespace Collagemanagement.web.Controllers
{

    [Area("Admin")]

    public class teacherController : Controller
    {
        private ITeacherServices _teacherServices;
        private ISessionServices _sessionServices;

        public teacherController(ITeacherServices teacherServices, ISessionServices sessionServices)
        {
            _teacherServices = teacherServices;
            _sessionServices = sessionServices;
        }
        [HttpGet]
        public IActionResult Index()
        {
            ViewBag.session = new SelectList(_sessionServices.GetAll(), "Id", "Combined");
            var teacher = _teacherServices.GetAllTeacher(pageNumber: 0, pageSize: 10);
            return View(teacher);
        }
        [HttpGet]
        public IActionResult AddTeacher()
        {
            return View();
        }
        public async Task<IActionResult> AddTeacher(CreateTeacherViewModel vm)
        {
            await _teacherServices.AddTeacher(vm);
            return RedirectToAction("Index");

        }
        public IActionResult EnrollStudent(PageResult<TeacherViewModel> vm, int SessionId, bool SelectAll)
        {
            if (SessionId == 0 || SessionId == -1)
            {
                TempData["error"] = "Please Select Session";
                return RedirectToAction("Index");
            }
            var TeacherLsit = vm.Data.Where(x => x.Selected == true).Select(y => y.Id).ToList();
            var result = _teacherServices.AddSessionwithTeacher(SessionId, TeacherLsit);
            if (result == 0)
            {
                TempData["error"] = "Sorry Duplicates are not Added";

            }
            if (result > 0)

                TempData["sucess"] = $"No of ({result}) records are added successful";
            return RedirectToAction("Index");



        }
    }
}
