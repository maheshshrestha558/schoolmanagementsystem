﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Schoolmanage.Services.Services;
using Schoolmanage.Utilities;
using Schoolmanage.ViewModel.Viewmodel;

namespace Collagemanagement.web.Controllers
{
    public class StudentController : Controller
    {
        private IStudentServices _studentServices;
        private IGradeServices _gradeServices;
        private ISessionServices _SessionServices;



        public StudentController(
            IStudentServices studentServices,
            IGradeServices gradeServices,
            ISessionServices sessionServices
            )
        {
            _studentServices = studentServices;
            _gradeServices = gradeServices;
            _SessionServices = sessionServices;
        }

        public IActionResult Index(int pageNumber = 1, int pageSize = 10)
        {
            ViewBag.Grades = new SelectList(_gradeServices.GetAll(), "Id", "Name");
            ViewBag.Session = new SelectList(_SessionServices.GetAll(), "Id", "Combined");
            var student = _studentServices.GetAll(pageNumber, pageSize);
            return View(student);
        }
        [HttpPost]
        public IActionResult EnrollStudent(PageResult<StudentViewModel> vm, int GradeId, int SessionId, bool SelectAll)
        {
            if (GradeId == 0 || GradeId == -1 || SessionId == 0 || SessionId == -1)
            {
                TempData["error"] = "Please Select Both Grade and Session";
                return RedirectToAction("Index");
            }
            var studenIdList = vm.Data.Where(x => x.Selected == true).Select(y => y.Id).ToList();
            GradeViewModel grade = _gradeServices.GetById(GradeId);
            var result = _gradeServices.AddGradeWithStudent(grade, SessionId, studenIdList);
            if (result == 0)
            {
                TempData["error"] = "Sorry Duplicates are not Added";

            }
            if (result > 0)

                TempData["success"] = $"No of ({result}) records are added successful";
            return RedirectToAction("Index");



        }

        [HttpGet]
        public async Task<IActionResult> AddStudent()
        {
            return View();
        }

        [HttpPost]

        public async Task<IActionResult> AddStudent(CreateStudentViewModel vm)
        {

            await _studentServices.AddStudent(vm);
            return View();
        }
    }
}
