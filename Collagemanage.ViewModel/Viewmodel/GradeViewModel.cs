﻿using schoolmanage.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoolmanage.ViewModel.Viewmodel
{
    public class GradeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public GradeViewModel()
        {

            
        }
        public GradeViewModel(Grade grade)
        {
            Id = grade.GradeId;
            Name = grade.Name;
        }
        //private List<SessionViewModel> ConvertToViewModelList(List<YearlySession> modelList)
        //{
        //    return modelList.Select(x=>new SessionViewModel(x)).ToList();
        //}

        public Grade Convert(GradeViewModel g)
        {
            return new Grade { GradeId = g.Id, Name = g.Name, };
        }



    }
}
