﻿using schoolmanage.model;
using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoolmanage.ViewModel.Viewmodel
{
    public class TeacherViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public DateTime DateofJoin { get; set; }
        public bool Selected { get; set; }
        public string KeyId { get; set; }
        public string Qualification { get; set; }
        public int YearofEx { get; set; }

        public TeacherViewModel()
        {

        }
        public TeacherViewModel(Teacher model)
        {
            Id=model.TeacherId;
            FirstName=model.FirstName;
            LastName=model.LastName;
            DOB=model.DOB;
            DateofJoin=model.DateofJoin;
            Selected = model.Selected;
            KeyId = model.KeyId;
            Qualification = model.Qualification;
            YearofEx = model.YearofEx;
        }
    }
}
