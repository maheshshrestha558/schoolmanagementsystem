﻿using schoolmanage.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoolmanage.ViewModel.Viewmodel
{
    public class StudentViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public DateTime DateofJoin { get; set; } = DateTime.Now;
        public bool Selected { get; set; }
        public string KeyId { get; set; }

        public string SelectAll { get; set; }
        public StudentViewModel()
        {
            
        }

        public StudentViewModel(Student model)
        {
            Id=model.StudentId;
            FirstName=model.FirstName;
            LastName=model.LastName;
            DOB = model.DOB;
            DateofJoin = model.DateofJoin;
            KeyId = model.KeyId;
        }
    }
}
