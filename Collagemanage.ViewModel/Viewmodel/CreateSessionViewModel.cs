﻿using schoolmanage.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoolmanage.ViewModel.Viewmodel
{
    public class CreateSessionViewModel
    {

        public string start { get; set; }
        public string end { get; set; }
        public Session convert(CreateSessionViewModel vm)
        {
            return new Session
            {
                start = vm.start,
                End = vm.end,

            };
        }


    }
}
