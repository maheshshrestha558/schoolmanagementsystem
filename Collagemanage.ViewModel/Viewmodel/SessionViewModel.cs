﻿
using schoolmanage.model;

namespace Schoolmanage.ViewModel.Viewmodel
{
    public class SessionViewModel
    {
        public int Id { get; set; }
        public string start { get; set; }
        public string End { get; set; }

        public List<GradeViewModel> gradeViewModels { get; set; }
        public string Combined
        {
            get
            {
                return start + "-" + End;
            }
        }

        public SessionViewModel(Session model)
        {
            Id = model.SessionId;
            start = model.start;
            End = model.End;
        }
        public SessionViewModel()
        {
            
        }
    }
}