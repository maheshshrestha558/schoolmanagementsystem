﻿using schoolmanage.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoolmanage.ViewModel.Viewmodel
{
    public class CreateGradeViewModel
    {
        public string Name { get; set; }
        public Grade Convert(CreateGradeViewModel gm)
        {
            return new Grade
            {
                Name = gm.Name
            };
        }

        public Session Convert(CreateSessionViewModel vm)
        {
            throw new NotImplementedException();
        }
    }

}
