﻿using Collagemanage.Model;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using schoolmanage.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collagemanage.Repositoy
{
    public class Applicationdbcontext : IdentityDbContext
    {
        public Applicationdbcontext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<Grade> Grades { get; set; }
        public DbSet<Enroll> Enrolls { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            builder.Entity<AssignGrade>().HasOne(x => x.Grade).
                WithMany(z => z.AssignGrades).HasForeignKey(x => x.GradeId).OnDelete(DeleteBehavior.SetNull);

            builder.Entity<AssignGrade>().HasOne(x => x.Teacher).
                WithMany(z => z.AssignGrades).HasForeignKey(x => x.TeaceherId).OnDelete(DeleteBehavior.SetNull);

            builder.Entity<Enroll>().HasOne(x => x.Student).
                WithMany(z => z.YearlySession).HasForeignKey(x => x.StudentId).OnDelete(DeleteBehavior.SetNull);

            builder.Entity<Enroll>().HasOne(x => x.Session).
                WithMany(z => z.Enrolls).HasForeignKey(x => x.SessionId).OnDelete(DeleteBehavior.SetNull);

            builder.Entity<Enroll>().HasOne(x => x.Grade).
                WithMany(z => z.Enrolls).HasForeignKey(x => x.GradeId).OnDelete(DeleteBehavior.SetNull);

            builder.Entity<TeacherSession>().HasOne(x => x.Teacher).
                WithMany(z => z.TeacherSessions).HasForeignKey(x => x.TeacherId).OnDelete(DeleteBehavior.SetNull);

            builder.Entity<TeacherSession>().HasOne(x => x.Session).
                WithMany(z => z.teacherSessions).HasForeignKey(x => x.SessionId).OnDelete(DeleteBehavior.SetNull);




            base.OnModelCreating(builder);

        }
    }
}
