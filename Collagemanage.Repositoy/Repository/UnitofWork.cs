﻿using Collagemanage.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoolmanage.Repository.Repository
{
    public class UnitofWork : IUnitofWork, IDisposable 
    {
        private bool disposedValue=false;
        private readonly Applicationdbcontext _context;

        public UnitofWork(Applicationdbcontext context)
        {
            _context = context;
        }

        
        
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _context.Dispose();
                }

                
                disposedValue = true;
            }
        }

        public IGenreRepository<T> GenreRepository<T>() where T : class
        {

            IGenreRepository<T> repo = new GenreRepository<T>(_context);
            return repo;
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
