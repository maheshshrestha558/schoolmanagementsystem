﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoolmanage.Repository.Repository
{
    public interface IUnitofWork
    {
        IGenreRepository<T> GenreRepository<T>() where T : class;
        void Save();
    }
}
