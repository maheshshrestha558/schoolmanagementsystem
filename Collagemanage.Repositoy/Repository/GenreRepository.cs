﻿using Collagemanage.Repositoy;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Schoolmanage.Repository.Repository
{
    public class GenreRepository<T> : IDisposable, IGenreRepository<T> where T : class
    {
        private readonly Applicationdbcontext _context;
        internal DbSet<T> dbset;

        public GenreRepository(Applicationdbcontext context)
        {
            _context = context;
            dbset = _context.Set<T>();
        }
        private bool disposedValue;

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                disposedValue = true;
            }
        }


        public IEnumerable<T> GetAll(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderby = null, Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null, bool disableTracking = true)
        {
            IQueryable<T> query = dbset;
            if (disableTracking)
            {
                query = query.AsNoTracking();
            }
            if (filter != null)
            {
                query=query.Where(filter);
            }

            if (filter != null)
            {
                query = include(query);

            }
            if (orderby != null)
            {
                return orderby(query).ToList();
            }
            else
            {
                return query.ToList();
            }

        }

        public T GetById(int id)
        {
            return dbset.Find(id);
        }

        public T GetByIdAsync(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderby = null, Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null, bool disableTracking = true)
        {
            IQueryable<T> query = dbset;
            if (disableTracking)
            {
                query = query.AsNoTracking();
            }
            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (filter != null)
            {
                query = include(query);

            }
            return query.FirstOrDefault();

        }

        public void Add(T entity)
        {
            dbset.Add(entity);
        }

        public async Task<T> AddAsync(T entity)
        {
            dbset.Add(entity);
            return entity;
        }

        public void AddRange(List<T> entity)
        {
            dbset.AddRange(entity);
        }

        public void Update(T entity)
        {
            dbset.Attach(entity);
            _context.Entry(entity).State=EntityState.Modified;
        }

        public async Task<T> UpdateAsync(T entity)
        {
            dbset.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        public void Delete(T entity)
        {
            if(_context.Entry(entity).State == EntityState.Detached)
            {
                dbset.Attach(entity);

            }
            dbset.Remove(entity);
        }


        public async  Task<T> DeleteAsync(T entity)
        {
            if (_context.Entry(entity).State == EntityState.Detached)
            {
                dbset.Attach(entity);

            }
            dbset.Remove(entity);
            return entity;

        }

        public void DeleteRange(List<T> entityList)
        {
            dbset.RemoveRange(entityList);
        }

        public bool Exists(Expression<Func<T, bool>> filter = null)
        {
           return dbset.Any(filter);

        }

        
    }
}
