﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoolmanage.Utilities
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}
