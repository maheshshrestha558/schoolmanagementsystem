﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoolmanage.Utilities
{
    public static class WebSideRole
    {
        public const string WebSite_Admin = "Admin";
        public const string Website_Teacher = "Teacher";
        public const string WebSite_Student = "Student";
        public const string WebSite_Employee = "Employee";

    }
}
