﻿using Microsoft.AspNetCore.Identity;
using schoolmanage.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Collagemanage.Model;
using Collagemanage.Repositoy;

namespace Schoolmanage.Utilities
{
    public class DbInitializer : IDbInitializer
    {
        private UserManager<ApplicationUser> _userManager;
        private RoleManager<IdentityRole> _roleManager;
        private Applicationdbcontext _context;

        public DbInitializer(UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager,
           Applicationdbcontext context)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
        }

        public void Initialize()
        {
            try
            {
                if (_context.Database.GetPendingMigrations().Count() > 0)
                {
                    _context.Database.Migrate();
                }
            }
            catch
            {
                throw;

            }
            if (!_roleManager.RoleExistsAsync(WebSideRole.WebSite_Admin).GetAwaiter().GetResult())
            {
                _roleManager.CreateAsync(new IdentityRole(WebSideRole.WebSite_Admin)).GetAwaiter().GetResult();
                _roleManager.CreateAsync(new IdentityRole(WebSideRole.WebSite_Employee)).GetAwaiter().GetResult();
                _roleManager.CreateAsync(new IdentityRole(WebSideRole.WebSite_Student)).GetAwaiter().GetResult();
                _roleManager.CreateAsync(new IdentityRole(WebSideRole.Website_Teacher)).GetAwaiter().GetResult();

                _userManager.CreateAsync(new ApplicationUser
                {
                    UserName = "admin@gmail.com",
                    Email = "admin@gmail.com"
                },
                    "Admin@123"
                ).GetAwaiter().GetResult();

                var appuser = _userManager.Users.Where(x => x.Email == "admin@gmail.com").FirstOrDefault();

                if (appuser != null)
                {
                    _userManager.AddToRoleAsync(appuser, WebSideRole.WebSite_Admin).GetAwaiter().GetResult();
                }
            }
        }
    }
}
